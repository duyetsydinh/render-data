
import React, { Component } from 'react';

class App extends Component {

  constructor() {
      super();

      this.state = {
          users: []
      }
  }

  async componentDidMount() {
    await fetch('https://5fc3323b9210060016869f8a.mockapi.io/api/flower-v1/user')
        .then(res => res.json())
        .then((data) => {
            this.setState({ users: data });
            console.log(data)
        })
        .catch(console.log)
  }

  renderUsers = () => {
      let users = this.state.users.map((data, index) =>
          <tr key={data.id}>
            <td>{data.id}</td>
            <td>{data.name}</td>
            <td>{data.email}</td>
            <td>{data.website}</td>
          </tr>
      );

      return users;
  }

  render() {
    return (
      <div className="App">
        <div className="container">
          <h1 className="text-center">Users List</h1>

          <table className="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th>Website</th>
              </tr>
            </thead>
            <tbody>
              {this.renderUsers()}
            </tbody>
          </table>

        </div>
      </div>
    );
  }
}

export default App;
